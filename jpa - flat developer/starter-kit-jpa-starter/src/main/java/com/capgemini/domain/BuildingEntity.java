package com.capgemini.domain;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.capgemini.datatype.Localization;

@Entity
@Table(name = "building")
public class BuildingEntity extends AbstractEntity {

	@Column(name = "description", length = 300)
	private String description;

	@Column(name = "number_of_floors", nullable = false)
	private int numberOfFloors;

	@Column(name = "is_lift_equipped", nullable = false)
	private boolean isLiftEquipped;

	@Column(name = "number_of_flats", nullable = false)
	private int numberOfFlats;

	@OneToMany
	private List<FlatEntity> flats;

	public BuildingEntity() {

	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getNumberOfFloors() {
		return numberOfFloors;
	}

	public void setNumberOfFloors(int numberOfFloors) {
		this.numberOfFloors = numberOfFloors;
	}

	public boolean isLiftEquipped() {
		return isLiftEquipped;
	}

	public void setLiftEquipped(boolean isLiftEquipped) {
		this.isLiftEquipped = isLiftEquipped;
	}

	public int getNumberOfFlats() {
		return numberOfFlats;
	}

	public void setNumberOfFlats(int numberOfFlats) {
		this.numberOfFlats = numberOfFlats;
	}

	public List<FlatEntity> getFlats() {
		return flats;
	}

	public void setFlats(List<FlatEntity> flats) {
		this.flats = flats;
	}

	public Localization getLocalization() {
		return localization;
	}

	public void setLocalization(Localization localization) {
		this.localization = localization;
	}

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "city", column = @Column(name = "city", nullable = false) ),
			@AttributeOverride(name = "street", column = @Column(name = "street", nullable = false) ) })
	private Localization localization;

}

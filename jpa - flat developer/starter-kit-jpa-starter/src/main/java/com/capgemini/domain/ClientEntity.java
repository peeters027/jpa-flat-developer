package com.capgemini.domain;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.capgemini.datatype.Address;
import com.capgemini.datatype.Contact;

@Entity
@Table(name = "clients")
public class ClientEntity extends AbstractEntity {

	@Column(length = 19, nullable = false)
	private String creditCard;

	@ManyToMany
	private List<FlatEntity> flats;

	public ClientEntity() {

	}

	public String getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(String creditCard) {
		this.creditCard = creditCard;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

	public List<FlatEntity> getFlats() {
		return flats;
	}

	public void setFlats(List<FlatEntity> flats) {
		this.flats = flats;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "mail", column = @Column(name = "mail", nullable = false) ),
			@AttributeOverride(name = "telephone", column = @Column(name = "telephone", length = 11, nullable = false) ) })
	private Contact contact;

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "street", column = @Column(name = "street", nullable = false) ),
			@AttributeOverride(name = "buildingNumber", column = @Column(name = "building_number", nullable = false) ),
			@AttributeOverride(name = "flatNumber", column = @Column(name = "flat_number") ),
			@AttributeOverride(name = "postCode", column = @Column(name = "post_code", nullable = false) ),
			@AttributeOverride(name = "city", column = @Column(name = "city", nullable = false) ) })
	private Address address;
}

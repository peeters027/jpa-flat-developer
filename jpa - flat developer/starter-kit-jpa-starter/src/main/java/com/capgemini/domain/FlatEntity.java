package com.capgemini.domain;

import java.util.List;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;

import com.capgemini.datatype.Address;
import com.capgemini.datatype.Status;

@Entity
@Table(name = "flats")
public class FlatEntity extends AbstractEntity {

	@Column(name = "size", precision = 10, scale = 2, nullable = false)
	private double sizeInSquareMeters;

	@Column(name = "number_of_rooms", nullable = false)
	private int numberOfRooms;

	@Column(name = "number_of_balconies", nullable = false)
	private int numberOfBalconies;

	@Column(name = "floor_number", nullable = false)
	private int floorNumber;

	@Enumerated(EnumType.STRING)
	private Status status;

	@Column(name = "price", precision = 10, scale = 2, nullable = false)
	private double price;

	@ManyToMany(mappedBy = "flats")
	private List<ClientEntity> clients;

	@PreRemove
	public void setNullBeforeDelete() {
		clients.forEach(client -> client.setFlats(null));
	}

	@ManyToOne
	private BuildingEntity building;

	public int getNumberOfRooms() {
		return numberOfRooms;
	}

	public void setNumberOfRooms(int numberOfRooms) {
		this.numberOfRooms = numberOfRooms;
	}

	public int getNumberOfBalconies() {
		return numberOfBalconies;
	}

	public void setNumberOfBalconies(int numberOfBalconies) {
		this.numberOfBalconies = numberOfBalconies;
	}

	public int getFloorNumber() {
		return floorNumber;
	}

	public void setFloorNumber(int floorNumber) {
		this.floorNumber = floorNumber;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public double getSizeInSquareMeters() {
		return sizeInSquareMeters;
	}

	public void setSizeInSquareMeters(double sizeInSquareMeters) {
		this.sizeInSquareMeters = sizeInSquareMeters;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public List<ClientEntity> getClients() {
		return clients;
	}

	public void setClients(List<ClientEntity> clients) {
		this.clients = clients;
	}

	public BuildingEntity getBuilding() {
		return building;
	}

	public void setBuilding(BuildingEntity building) {
		this.building = building;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Embedded
	@AttributeOverrides({ @AttributeOverride(name = "street", column = @Column(name = "street", nullable = false) ),
			@AttributeOverride(name = "buildingNumber", column = @Column(name = "building_number", nullable = false) ),
			@AttributeOverride(name = "flatNumber", column = @Column(name = "flat_number", nullable = false) ),
			@AttributeOverride(name = "postCode", column = @Column(name = "post_code", nullable = false) ),
			@AttributeOverride(name = "city", column = @Column(name = "city", nullable = false) ) })
	private Address address;
}

package com.capgemini.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.ClientDao;
import com.capgemini.datatype.Status;
import com.capgemini.domain.ClientEntity;
import com.capgemini.domain.QClientEntity;
import com.capgemini.domain.QFlatEntity;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Repository
public class ClientDaoImpl extends AbstractDao<ClientEntity, Long> implements ClientDao {

	@Override
	public List<ClientEntity> getAllClientsWhichHaveMoreThanOneFlat() {
		QFlatEntity flatEntity = QFlatEntity.flatEntity;
		QClientEntity clientEntity = QClientEntity.clientEntity;
		JPAQueryFactory query = new JPAQueryFactory(entityManager);

		return query.from(clientEntity).select(clientEntity).innerJoin(clientEntity.flats, flatEntity)
				.where(flatEntity.status.eq(Status.SOLD)).groupBy(clientEntity.id).having(flatEntity.id.count().gt(1))
				.fetch();
	}
}

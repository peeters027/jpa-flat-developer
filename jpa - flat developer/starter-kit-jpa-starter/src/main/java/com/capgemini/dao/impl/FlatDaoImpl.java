package com.capgemini.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.assertj.core.util.Lists;
import org.springframework.stereotype.Repository;
import javax.persistence.criteria.Predicate;

import com.capgemini.dao.FlatDao;
import com.capgemini.datatype.Status;
import com.capgemini.domain.FlatEntity;
import com.capgemini.domain.FlatEntity_;
import com.capgemini.domain.QBuildingEntity;
import com.capgemini.domain.QClientEntity;
import com.capgemini.domain.QFlatEntity;
import com.capgemini.searchcriteria.FlatSearchCriteria;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Repository
public class FlatDaoImpl extends AbstractDao<FlatEntity, Long> implements FlatDao {

	@PersistenceContext
	private EntityManager entityManager;

	public List<FlatEntity> findFlatBySearchCriteria(FlatSearchCriteria flatSearchCriteria) {

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<FlatEntity> criteriaQuery = criteriaBuilder.createQuery(FlatEntity.class);
		List<Predicate> predicates = Lists.newArrayList();
		Root<FlatEntity> root = criteriaQuery.from(FlatEntity.class);

		Predicate statusPredicate = criteriaBuilder.notEqual(root.get("status"), Status.SOLD);
		predicates.add(statusPredicate);
		predicates.addAll(buildPredicates(flatSearchCriteria, criteriaBuilder, root));

		Predicate[] predicateArray = predicates.toArray(new Predicate[predicates.size()]);
		criteriaQuery.where(predicateArray);
		TypedQuery<FlatEntity> query = entityManager.createQuery(criteriaQuery);
		List<FlatEntity> resultList = query.getResultList();
		return resultList;

	}

	private List<Predicate> buildPredicates(FlatSearchCriteria flatSearchCriteria, CriteriaBuilder criteriaBuilder,
			Root<FlatEntity> root) {
		List<Predicate> predicates = Lists.newArrayList();

		if (flatSearchCriteria.getNumberOfBalcioniesFrom() != 0) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(FlatEntity_.numberOfBalconies),
					flatSearchCriteria.getNumberOfBalcioniesFrom()));
		}

		if (flatSearchCriteria.getNumberOfBalcioniesTo() != 0) {
			predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(FlatEntity_.numberOfBalconies),
					flatSearchCriteria.getNumberOfBalcioniesTo()));
		}

		if (flatSearchCriteria.getNumberOfRoomsFrom() != 0) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(FlatEntity_.numberOfRooms),
					flatSearchCriteria.getNumberOfRoomsFrom()));
		}

		if (flatSearchCriteria.getNumberOfRoomsTo() != 0) {
			predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(FlatEntity_.numberOfRooms),
					flatSearchCriteria.getNumberOfRoomsTo()));
		}

		if (flatSearchCriteria.getSizeFrom() != 0) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(FlatEntity_.sizeInSquareMeters),
					flatSearchCriteria.getSizeFrom()));
		}

		if (flatSearchCriteria.getSizeTo() != 0) {
			predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(FlatEntity_.sizeInSquareMeters),
					flatSearchCriteria.getSizeTo()));
		}
		return predicates;
	}

	@Override
	public double getSumOfFlatPricesByClient(Long clientId) {
		QFlatEntity flatEntity = QFlatEntity.flatEntity;
		QClientEntity clientEntity = QClientEntity.clientEntity;
		JPAQueryFactory query = new JPAQueryFactory(entityManager);

		Double sum = query.from(flatEntity).select(flatEntity.price.sum()).innerJoin(flatEntity.clients, clientEntity)
				.where(clientEntity.id.eq(clientId).and(flatEntity.status.eq(Status.SOLD))).fetchOne();

		if (sum == null) {
			sum = (double) 0;
		}
		return sum;
	}

	@Override
	public double getAveragePriceOfFlatsInBuilding(Long buildingId) {
		QFlatEntity flatEntity = QFlatEntity.flatEntity;
		QBuildingEntity buildingEntity = QBuildingEntity.buildingEntity;
		JPAQueryFactory query = new JPAQueryFactory(entityManager);

		Double avg = query.from(flatEntity).select(flatEntity.price.avg())
				.innerJoin(flatEntity.building, buildingEntity).where(buildingEntity.id.eq(buildingId)).fetchOne();

		if (avg == null) {
			avg = (double) 0;
		}
		return avg;
	}

	@Override
	public long getNumberOfFlatsInBuildingWithSpecifiedStatus(Status status, Long buildingId) {
		QFlatEntity flatEntity = QFlatEntity.flatEntity;
		QBuildingEntity buildingEntity = QBuildingEntity.buildingEntity;
		JPAQueryFactory query = new JPAQueryFactory(entityManager);

		return query.from(flatEntity).select(flatEntity.id.count()).innerJoin(flatEntity.building, buildingEntity)
				.where(buildingEntity.id.eq(buildingId).and(flatEntity.status.eq(status))).fetchCount();
	}

	@Override
	public List<FlatEntity> getAllFlatsDestinatedForDisabledPeople() {

		QFlatEntity flatEntity = QFlatEntity.flatEntity;
		QBuildingEntity buildingEntity = QBuildingEntity.buildingEntity;
		JPAQueryFactory query = new JPAQueryFactory(entityManager);

		return query.from(flatEntity)
				.select(flatEntity).innerJoin(flatEntity.building, buildingEntity).where(buildingEntity.isLiftEquipped
						.eq(true).or(buildingEntity.isLiftEquipped.eq(false).and(flatEntity.floorNumber.eq(0))))
				.fetch();
	}

}

package com.capgemini.dao;

import java.util.List;

import com.capgemini.domain.BuildingEntity;

public interface BuildingDao extends Dao<BuildingEntity, Long> {

	List<BuildingEntity> getBuildingWithTheBiggestNumberOfFreeFlats();

}

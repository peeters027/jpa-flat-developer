package com.capgemini.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.capgemini.dao.BuildingDao;
import com.capgemini.datatype.Status;
import com.capgemini.domain.BuildingEntity;
import com.capgemini.domain.QBuildingEntity;
import com.capgemini.domain.QFlatEntity;
import com.querydsl.jpa.impl.JPAQueryFactory;

@Repository
public class BuildingDaoImpl extends AbstractDao<BuildingEntity, Long> implements BuildingDao {

	@Override
	public List<BuildingEntity> getBuildingWithTheBiggestNumberOfFreeFlats() {

		QFlatEntity flatEntity = QFlatEntity.flatEntity;
		QBuildingEntity buildingEntity = QBuildingEntity.buildingEntity;
		JPAQueryFactory query = new JPAQueryFactory(entityManager);

		return query.from(buildingEntity).select(buildingEntity).innerJoin(buildingEntity.flats, flatEntity)
				.where(flatEntity.status.eq(Status.FREE)).groupBy(buildingEntity.id)
				.having(flatEntity.id.count()
						.eq(query.select(flatEntity.id.count()).from(flatEntity)
								.where(flatEntity.status.eq(Status.FREE)).groupBy(flatEntity.building.id)
								.orderBy(flatEntity.id.count().desc()).limit(1).fetchOne()))
				.fetch();
	}
}

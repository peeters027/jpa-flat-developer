package com.capgemini.dao;

import java.util.List;

import com.capgemini.domain.ClientEntity;

public interface ClientDao extends Dao<ClientEntity, Long> {

	List<ClientEntity> getAllClientsWhichHaveMoreThanOneFlat();

}

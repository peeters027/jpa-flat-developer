package com.capgemini.dao;

import java.util.List;

import com.capgemini.datatype.Status;
import com.capgemini.domain.FlatEntity;
import com.capgemini.searchcriteria.FlatSearchCriteria;

public interface FlatDao extends Dao<FlatEntity, Long> {

	List<FlatEntity> findFlatBySearchCriteria(FlatSearchCriteria flatSearchCriteria);

	double getSumOfFlatPricesByClient(Long clientId);

	double getAveragePriceOfFlatsInBuilding(Long buildingId);

	long getNumberOfFlatsInBuildingWithSpecifiedStatus(Status status, Long buildingId);

	List<FlatEntity> getAllFlatsDestinatedForDisabledPeople();
}

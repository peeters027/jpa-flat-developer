package com.capgemini.datatype;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Localization {

	@Column(name = "city", length = 30, nullable = false)
	private String city;

	@Column(name = "street", length = 30, nullable = false)
	private String street;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

}

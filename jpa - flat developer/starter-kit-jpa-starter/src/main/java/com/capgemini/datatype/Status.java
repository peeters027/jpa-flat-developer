package com.capgemini.datatype;

public enum Status {
	FREE, RESERVED, SOLD;
}

package com.capgemini.exception;

public class FlatExistenceException extends BusinessException {

	private static final long serialVersionUID = 1L;

	private static final String MESSAGE = "Given flat does not exist";

	public FlatExistenceException() {
		super(MESSAGE);
	}
}

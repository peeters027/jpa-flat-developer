package com.capgemini.exception;

public class FlatInvalidDataException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public FlatInvalidDataException(String message) {
		super(message);
	}
}

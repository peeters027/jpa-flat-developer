package com.capgemini.searchcriteria;

public class FlatSearchCriteria {

	private double sizeFrom;

	private double sizeTo;

	private int numberOfRoomsFrom;

	private int numberOfRoomsTo;

	private int numberOfBalcioniesFrom;

	private int numberOfBalcioniesTo;

	public double getSizeFrom() {
		return sizeFrom;
	}

	public void setSizeFrom(double sizeFrom) {
		this.sizeFrom = sizeFrom;
	}

	public double getSizeTo() {
		return sizeTo;
	}

	public void setSizeTo(double sizeTo) {
		this.sizeTo = sizeTo;
	}

	public int getNumberOfRoomsFrom() {
		return numberOfRoomsFrom;
	}

	public void setNumberOfRoomsFrom(int numberOfRoomsFrom) {
		this.numberOfRoomsFrom = numberOfRoomsFrom;
	}

	public int getNumberOfRoomsTo() {
		return numberOfRoomsTo;
	}

	public void setNumberOfRoomsTo(int numberOfRoomsTo) {
		this.numberOfRoomsTo = numberOfRoomsTo;
	}

	public int getNumberOfBalcioniesFrom() {
		return numberOfBalcioniesFrom;
	}

	public void setNumberOfBalcioniesFrom(int numberOfBalcioniesFrom) {
		this.numberOfBalcioniesFrom = numberOfBalcioniesFrom;
	}

	public int getNumberOfBalcioniesTo() {
		return numberOfBalcioniesTo;
	}

	public void setNumberOfBalcioniesTo(int numberOfBalcioniesTo) {
		this.numberOfBalcioniesTo = numberOfBalcioniesTo;
	}

}

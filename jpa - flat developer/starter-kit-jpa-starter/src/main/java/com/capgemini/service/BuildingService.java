package com.capgemini.service;

import java.util.List;

import com.capgemini.domain.BuildingEntity;

public interface BuildingService {

	List<BuildingEntity> getBuildingWithTheBiggestNumberOfFreeFlats();

	BuildingEntity findOne(Long buildingId);
}

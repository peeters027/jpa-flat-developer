package com.capgemini.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capgemini.dao.BuildingDao;
import com.capgemini.domain.BuildingEntity;
import com.capgemini.service.BuildingService;

@Service
@Transactional(readOnly = true)
public class BuildingServiceImpl implements BuildingService {

	@Autowired
	BuildingDao buildingDao;

	@Override
	public List<BuildingEntity> getBuildingWithTheBiggestNumberOfFreeFlats() {
		return buildingDao.getBuildingWithTheBiggestNumberOfFreeFlats();
	}

	@Override
	public BuildingEntity findOne(Long buildingId) {
		return buildingDao.findOne(buildingId);
	}

}

package com.capgemini.service.impl;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.dao.ClientDao;
import com.capgemini.domain.ClientEntity;
import com.capgemini.exception.BusinessException;
import com.capgemini.service.ClientService;

@Service
@Transactional(readOnly = true)
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientDao clientDao;

	@Override
	@Transactional(readOnly = false)
	public ClientEntity save(ClientEntity client) {
		return client = clientDao.save(client);
	}

	@Override
	@Transactional(readOnly = false)
	public ClientEntity update(ClientEntity client) throws BusinessException {
		return clientDao.update(client);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(ClientEntity client) throws BusinessException {
		client.getFlats().remove(client);
		clientDao.delete(client.getId());

	}

	@Override
	public List<ClientEntity> getAllClientsWhichHaveMoreThanOneFlat() {
		return clientDao.getAllClientsWhichHaveMoreThanOneFlat();
	}

	@Override
	public ClientEntity findOne(Long clientId) {
		return clientDao.findOne(clientId);
	}

	@Override
	public List<ClientEntity> findAllClients() {
		return clientDao.findAll();
	}

}

package com.capgemini.service.impl;

import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.dao.FlatDao;
import com.capgemini.datatype.Status;
import com.capgemini.domain.BuildingEntity;
import com.capgemini.domain.ClientEntity;
import com.capgemini.domain.FlatEntity;
import com.capgemini.searchcriteria.FlatSearchCriteria;
import com.capgemini.service.FlatService;
import com.capgemini.validators.FlatValidator;

@Service
@Transactional(readOnly = true)
public class FlatServiceImpl implements FlatService {

	@Autowired
	private FlatDao flatDao;

	@Autowired
	private FlatValidator flatValidator;

	@Override
	@Transactional(readOnly = false)
	public FlatEntity save(FlatEntity flat) {
		flatValidator.validateFlatData(flat);
		return flatDao.save(flat);
	}

	@Override
	@Transactional(readOnly = false)
	public FlatEntity update(FlatEntity flat) {
		flatValidator.validateFlatExistence(flat.getId());
		return flatDao.update(flat);
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(FlatEntity flat) {
		flatValidator.validateFlatExistence(flat.getId());
		flat.getBuilding().getFlats().remove(flat);
		flatDao.delete(flat.getId());
	}

	@Override
	public List<FlatEntity> findFlatBySearchCriteria(FlatSearchCriteria flatSearchCriteria) {
		return flatDao.findFlatBySearchCriteria(flatSearchCriteria);
	}

	@Override
	public double getSumOfFlatPricesByClient(ClientEntity client) {
		return flatDao.getSumOfFlatPricesByClient(client.getId());
	}

	@Override
	public double getAveragePriceOfFlatsInBuilding(BuildingEntity building) {
		return flatDao.getAveragePriceOfFlatsInBuilding(building.getId());
	}

	@Override
	public long getNumberOfFlatsInBuildingWithSpecifiedStatus(Status status, BuildingEntity building) {
		return flatDao.getNumberOfFlatsInBuildingWithSpecifiedStatus(status, building.getId());
	}

	@Override
	public List<FlatEntity> getAllFlatsDestinatedForDisabledPeople() {
		return flatDao.getAllFlatsDestinatedForDisabledPeople();
	}

	@Override
	public FlatEntity findOne(Long flatId) {
		return flatDao.findOne(flatId);
	}

	@Override
	public List<FlatEntity> findAllFlats() {
		return flatDao.findAll();
	}
}

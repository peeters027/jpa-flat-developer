package com.capgemini.service;

import java.util.List;

import com.capgemini.datatype.Status;
import com.capgemini.domain.BuildingEntity;
import com.capgemini.domain.ClientEntity;
import com.capgemini.domain.FlatEntity;
import com.capgemini.searchcriteria.FlatSearchCriteria;

public interface FlatService {

	FlatEntity save(FlatEntity flat);

	FlatEntity update(FlatEntity flat);

	void delete(FlatEntity flat);

	FlatEntity findOne(Long flatId);

	List<FlatEntity> findAllFlats();

	List<FlatEntity> findFlatBySearchCriteria(FlatSearchCriteria flatSearchCriteria);

	double getSumOfFlatPricesByClient(ClientEntity client);

	double getAveragePriceOfFlatsInBuilding(BuildingEntity building);

	long getNumberOfFlatsInBuildingWithSpecifiedStatus(Status status, BuildingEntity building);

	List<FlatEntity> getAllFlatsDestinatedForDisabledPeople();

}

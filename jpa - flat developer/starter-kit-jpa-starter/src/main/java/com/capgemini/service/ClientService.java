package com.capgemini.service;

import java.util.List;

import com.capgemini.domain.ClientEntity;
import com.capgemini.exception.BusinessException;

public interface ClientService {

	ClientEntity save(ClientEntity client);

	ClientEntity update(ClientEntity client) throws BusinessException;

	void delete(ClientEntity client) throws BusinessException;

	List<ClientEntity> getAllClientsWhichHaveMoreThanOneFlat();

	ClientEntity findOne(Long clientId);

	List<ClientEntity> findAllClients();

}

package com.capgemini.validators;

import com.capgemini.domain.FlatEntity;

public interface FlatValidator {

	void validateFlatExistence(Long flatId);

	void validateFlatData(FlatEntity flat);

}

package com.capgemini.validators.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.dao.BuildingDao;
import com.capgemini.dao.FlatDao;
import com.capgemini.domain.FlatEntity;
import com.capgemini.exception.FlatExistenceException;
import com.capgemini.exception.FlatInvalidDataException;
import com.capgemini.validators.FlatValidator;

@Service
public class FlatValidatorImpl implements FlatValidator {

	@Autowired
	private FlatDao flatDao;

	@Autowired
	private BuildingDao buildingDao;

	public void validateFlatExistence(Long flatId) {
		if (!flatDao.exists(flatId)) {
			throw new FlatExistenceException();
		}
	};

	public void validateFlatData(FlatEntity flat) {
		validateBuildingExistence(flat.getBuilding().getId());
		validateFloorNumber(flat.getFloorNumber(), flat.getBuilding().getNumberOfFloors());
		validateBalconiesNumber(flat.getNumberOfBalconies());
		validateRoomsNumber(flat.getNumberOfRooms());
		validatePriceNumber(flat.getPrice());
		validateSizeNumber(flat.getSizeInSquareMeters());
	};

	private void validateBuildingExistence(Long buildingId) {
		if (!buildingDao.exists(buildingId)) {
			throw new FlatInvalidDataException("Given building does not exist");
		}
	};

	private void validateFloorNumber(int flatFloorNumber, int highestFloorNumberOfBuilding) {
		if (flatFloorNumber > highestFloorNumberOfBuilding || flatFloorNumber < 0) {
			throw new FlatInvalidDataException("Given floor number is invalid");
		}
	};

	private void validateRoomsNumber(int flatRoomsNumber) {
		if (flatRoomsNumber < 0) {
			throw new FlatInvalidDataException("Given number of rooms is invalid");
		}
	};

	private void validateBalconiesNumber(int balconiesNumber) {
		if (balconiesNumber < 0) {
			throw new FlatInvalidDataException("Given number of balconies is invalid");
		}
	};

	private void validatePriceNumber(double price) {
		if (price < 0) {
			throw new FlatInvalidDataException("Given price number is invalid");
		}
	};

	private void validateSizeNumber(double size) {
		if (size < 0) {
			throw new FlatInvalidDataException("Given size number is invalid");
		}
	};

}

package com.capgemini.service;

import java.util.List;

import static org.junit.Assert.*;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.dao.FlatDao;
import com.capgemini.datatype.Address;
import com.capgemini.datatype.Contact;
import com.capgemini.domain.ClientEntity;
import com.capgemini.domain.FlatEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ClientServiceTest {

	@Autowired
	private ClientService serviceUnderTest;

	@Autowired
	private FlatDao flatDao;

	@Test
	public void shouldGetAllClientsWhichBoughtMoreThenOneFlat() {

		// given when
		List<ClientEntity> clients = serviceUnderTest.getAllClientsWhichHaveMoreThanOneFlat();

		// then
		assertEquals(1, clients.size());
		assertTrue(clients.contains(serviceUnderTest.findOne(1L)));
		assertFalse(clients.contains(serviceUnderTest.findOne(2L)));

	}

	@Test
	public void shouldSaveClient() {

		// given
		List<ClientEntity> clients = serviceUnderTest.findAllClients();
		int countBefore = clients.size();
		ClientEntity clientToSave = getExampleClient();

		// when
		serviceUnderTest.save(clientToSave);

		// then
		assertEquals(countBefore + 1, serviceUnderTest.findAllClients().size());
		assertTrue(serviceUnderTest.findAllClients().contains(clientToSave));
	}

	@Test
	public void shouldDeleteClient() {

		// given
		List<ClientEntity> clients = serviceUnderTest.findAllClients();
		int countBefore = clients.size();
		ClientEntity clientToDelete = serviceUnderTest.findOne(1L);
		FlatEntity flat = flatDao.findOne(1L);

		// when
		serviceUnderTest.delete(clientToDelete);

		// then
		assertEquals(countBefore - 1, serviceUnderTest.findAllClients().size());
		assertFalse(serviceUnderTest.findAllClients().contains(clientToDelete));
		assertFalse(flat.getClients().contains(clientToDelete));
	}

	@Test
	public void shouldUpdateClient() {

		// given
		ClientEntity clientToUpdate = serviceUnderTest.findOne(1L);
		clientToUpdate.setCreditCard("2222-2222-2222-2222");

		// when
		serviceUnderTest.update(clientToUpdate);

		// then
		assertEquals(serviceUnderTest.findOne(1L).getCreditCard(), clientToUpdate.getCreditCard());
	}

	public Address getExampleAddress() {
		Address address = new Address();
		address.setBuildingNumber(1);
		address.setFlatNumber(1);
		address.setCity("Chicago");
		address.setStreet("Backer Street");
		address.setPostCode("10-100");
		return address;
	}

	public Contact getExampleContact() {
		Contact contact = new Contact();
		contact.setMail("example@gmail.com");
		contact.setTelephone("111-111-111");
		return contact;
	}

	public ClientEntity getExampleClient() {
		ClientEntity client = new ClientEntity();
		client.setAddress(getExampleAddress());
		client.setContact(getExampleContact());
		client.setCreditCard("1111-1111-1111-1111");
		return client;
	}
}

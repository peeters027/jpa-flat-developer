package com.capgemini.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BuildingServiceTest.class, ClientServiceTest.class, FlatServiceTest.class })
public class ServiceTestSuite {

}

package com.capgemini.service;

import static org.junit.Assert.*;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.dao.BuildingDao;
import com.capgemini.dao.ClientDao;
import com.capgemini.dao.FlatDao;
import com.capgemini.datatype.Address;
import com.capgemini.datatype.Contact;
import com.capgemini.datatype.Status;
import com.capgemini.domain.BuildingEntity;
import com.capgemini.domain.ClientEntity;
import com.capgemini.domain.FlatEntity;
import com.capgemini.exception.FlatExistenceException;
import com.capgemini.exception.FlatInvalidDataException;
import com.capgemini.searchcriteria.FlatSearchCriteria;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class FlatServiceTest {

	@Autowired
	private FlatService serviceUnderTest;

	@Autowired
	private FlatDao flatDao;

	@Autowired
	private ClientDao clientDao;

	@Autowired
	private BuildingDao buildingDao;

	@PersistenceContext
	protected EntityManager entityManager;

	@Test
	public void shouldGetSumOfClientsFlatPrices() {

		// given
		ClientEntity clientWithIdOne = clientDao.findOne(1L);
		ClientEntity clientWithIdTwo = clientDao.findOne(2L);
		ClientEntity nonExistingClient = getExampleClient();
		nonExistingClient.setId(100L);

		// when
		double valueOfFirstClientsFlats = serviceUnderTest.getSumOfFlatPricesByClient(clientWithIdOne);
		double valueOfSecondClientsFlats = serviceUnderTest.getSumOfFlatPricesByClient(clientWithIdTwo);
		double valueOfNonExistingClientsFlats = serviceUnderTest.getSumOfFlatPricesByClient(clientWithIdTwo);

		// then
		assertEquals(555799.1, valueOfFirstClientsFlats, 1);
		assertEquals(0.0, valueOfSecondClientsFlats, 1);
		assertEquals(0.0, valueOfNonExistingClientsFlats, 1);

	}

	@Test
	public void shouldGetAveragePricesOfFlatsInBuilding() {
		// given
		BuildingEntity buildingWithIdOne = buildingDao.findOne(1L);
		BuildingEntity buildingWithIdSix = buildingDao.findOne(6L);
		BuildingEntity nonExistingBuilding = new BuildingEntity();
		nonExistingBuilding.setId(100L);

		// when
		double avaragePriceOfFlatsInFirstBuilding = serviceUnderTest
				.getAveragePriceOfFlatsInBuilding(buildingWithIdOne);
		double avaragePriceOfFlatsInSixthBuilding = serviceUnderTest
				.getAveragePriceOfFlatsInBuilding(buildingWithIdSix);
		double avaragePriceOfFlatsInNonExistingBuilding = serviceUnderTest
				.getAveragePriceOfFlatsInBuilding(nonExistingBuilding);

		// then
		assertEquals(243957.36, avaragePriceOfFlatsInFirstBuilding, 1);
		assertEquals(203290.0, avaragePriceOfFlatsInSixthBuilding, 1);
		assertEquals(0.0, avaragePriceOfFlatsInNonExistingBuilding, 1);
	}

	@Test
	public void shouldGetNumberOfFlatsWithSpecifiedStatusInBuilding() {
		// given when
		BuildingEntity buildingWithIdOne = buildingDao.findOne(1L);
		BuildingEntity buildingWithIdSix = buildingDao.findOne(6L);

		// when
		long numberOfReservedFlatsInFirstBuilding = serviceUnderTest
				.getNumberOfFlatsInBuildingWithSpecifiedStatus(Status.RESERVED, buildingWithIdOne);
		long numberOfSoldFlatsInFirstBuilding = serviceUnderTest
				.getNumberOfFlatsInBuildingWithSpecifiedStatus(Status.SOLD, buildingWithIdOne);
		long numberOfFreeFlatsInSixthBuilding = serviceUnderTest
				.getNumberOfFlatsInBuildingWithSpecifiedStatus(Status.FREE, buildingWithIdSix);

		// then
		assertEquals(1, numberOfReservedFlatsInFirstBuilding);
		assertEquals(1, numberOfSoldFlatsInFirstBuilding);
		assertEquals(0, numberOfFreeFlatsInSixthBuilding);
	}

	@Test
	public void shouldGetFlatsDestinedForDisabledPeople() {

		// given when
		List<FlatEntity> flatsForDisabledPeople = serviceUnderTest.getAllFlatsDestinatedForDisabledPeople();

		// then
		assertEquals(5, flatsForDisabledPeople.size());
		assertTrue(flatsForDisabledPeople.contains(serviceUnderTest.findOne(2L)));
		assertTrue(flatsForDisabledPeople.contains(serviceUnderTest.findOne(3L)));
		assertTrue(flatsForDisabledPeople.contains(serviceUnderTest.findOne(5L)));
		assertFalse(flatsForDisabledPeople.contains(serviceUnderTest.findOne(6L)));
	}

	@Test
	public void shouldFindFlatsBySizeCriteria() {

		// given
		FlatSearchCriteria sizeCriteria = new FlatSearchCriteria();
		sizeCriteria.setSizeFrom(90);
		sizeCriteria.setSizeTo(100);

		// when
		List<FlatEntity> foundFlatsBySizeCritera = flatDao.findFlatBySearchCriteria(sizeCriteria);

		// then
		assertEquals(1, foundFlatsBySizeCritera.size());
		assertTrue(foundFlatsBySizeCritera.contains(flatDao.findOne(9L)));
		assertFalse(foundFlatsBySizeCritera.contains(flatDao.findOne(1L)));
	}

	@Test
	public void shouldFindFlatsByBalconiesCriteria() {

		// given
		FlatSearchCriteria balconiesCriteria = new FlatSearchCriteria();
		balconiesCriteria.setNumberOfBalcioniesFrom(2);
		balconiesCriteria.setNumberOfBalcioniesTo(3);

		// when
		List<FlatEntity> foundFlatsByBalconiesCritera = flatDao.findFlatBySearchCriteria(balconiesCriteria);

		// then
		assertEquals(3, foundFlatsByBalconiesCritera.size());
		assertTrue(foundFlatsByBalconiesCritera.contains(flatDao.findOne(1L)));
		assertFalse(foundFlatsByBalconiesCritera.contains(flatDao.findOne(4L)));
	}

	@Test
	public void shouldFindFlatsByRoomsCriteria() {

		// given
		FlatSearchCriteria roomsCriteria = new FlatSearchCriteria();
		roomsCriteria.setNumberOfRoomsFrom(5);
		roomsCriteria.setNumberOfRoomsTo(5);

		// when
		List<FlatEntity> foundFlatsByRoomsCritera = flatDao.findFlatBySearchCriteria(roomsCriteria);

		// then
		assertEquals(2, foundFlatsByRoomsCritera.size());
		assertTrue(foundFlatsByRoomsCritera.contains(flatDao.findOne(6L)));
		assertFalse(foundFlatsByRoomsCritera.contains(flatDao.findOne(1L)));
	}

	@Test
	public void shouldFindFlatsByAllCriteria() {

		// given
		FlatSearchCriteria searchCriteria = new FlatSearchCriteria();
		searchCriteria.setSizeFrom(50);
		searchCriteria.setSizeTo(70);
		searchCriteria.setNumberOfBalcioniesFrom(0);
		searchCriteria.setNumberOfBalcioniesTo(2);
		searchCriteria.setNumberOfRoomsFrom(0);
		searchCriteria.setNumberOfRoomsTo(2);

		// when
		List<FlatEntity> foundFlatsByCritera = flatDao.findFlatBySearchCriteria(searchCriteria);

		// then
		assertEquals(1, foundFlatsByCritera.size());
		assertTrue(foundFlatsByCritera.contains(flatDao.findOne(8L)));
	}

	@Test
	public void shouldFindFlatsWhenCriteriaAreNotSpecified() {

		// given
		FlatSearchCriteria searchCriteria = new FlatSearchCriteria();
		// when
		List<FlatEntity> foundFlatsByCritera = flatDao.findFlatBySearchCriteria(searchCriteria);

		// then
		assertEquals(7, foundFlatsByCritera.size());
		assertTrue(foundFlatsByCritera.contains(flatDao.findOne(1L)));
		assertFalse(foundFlatsByCritera.contains(flatDao.findOne(5L)));
	}

	@Test
	public void shouldSaveFlat() {

		// given
		List<FlatEntity> flats = serviceUnderTest.findAllFlats();
		int countBefore = flats.size();
		FlatEntity flatToSave = getExampleFlat();

		// when
		serviceUnderTest.save(flatToSave);

		// then
		assertEquals(countBefore + 1, serviceUnderTest.findAllFlats().size());
		assertTrue(serviceUnderTest.findAllFlats().contains(flatToSave));
	}

	@Test(expected = FlatInvalidDataException.class)
	public void shouldThrowFlatInvalidDataExceptionWhenSavingWithInvalidFloorNumber() {

		// given
		FlatEntity flatToSave = getExampleFlat();
		flatToSave.setFloorNumber(30);

		// when
		serviceUnderTest.save(flatToSave);

		// then
		fail("FlatInvalidDataException should have thrown");
	}

	@Test(expected = FlatInvalidDataException.class)
	public void shouldThrowFlatInvalidDataExceptionWhenSavingWithInvalidBalconiesNumber() {
		// given
		FlatEntity flatToSave = getExampleFlat();
		flatToSave.setNumberOfBalconies(-1);

		// when
		serviceUnderTest.save(flatToSave);

		// then
		fail("FlatInvalidDataException should have thrown");
	}

	@Test(expected = FlatInvalidDataException.class)
	public void shouldThrowFlatInvalidDataExceptionWhenSavingWithInvalidBuilding() {

		// given
		FlatEntity flatToSave = getExampleFlat();
		flatToSave.getBuilding().setId(100L);

		// when
		serviceUnderTest.save(flatToSave);

		// then
		fail("FlatInvalidDataException should have thrown");
	}

	@Test(expected = FlatInvalidDataException.class)
	public void shouldThrowFlatInvalidDataExceptionWhenSavingWithInvalidSizeNumber() {

		// given
		FlatEntity flatToSave = getExampleFlat();
		flatToSave.setSizeInSquareMeters(-1);

		// when
		serviceUnderTest.save(flatToSave);

		// then
		fail("FlatInvalidDataException should have thrown");
	}

	@Test(expected = FlatInvalidDataException.class)
	public void shouldThrowFlatInvalidDataExceptionWhenSavingWithInvalidPrice() {

		// given
		FlatEntity flatToSave = getExampleFlat();
		flatToSave.setPrice(-1);

		// when
		serviceUnderTest.save(flatToSave);

		// then
		fail("FlatInvalidDataException should have thrown");
	}

	@Test
	public void shouldDeleteFlat() {

		// given
		List<FlatEntity> flats = serviceUnderTest.findAllFlats();
		int countBefore = flats.size();
		FlatEntity flatToDelete = serviceUnderTest.findOne(1L);
		BuildingEntity building = buildingDao.findOne(3L);

		// when
		serviceUnderTest.delete(flatToDelete);

		// then
		assertEquals(countBefore - 1, serviceUnderTest.findAllFlats().size());
		assertFalse(serviceUnderTest.findAllFlats().contains(flatToDelete));
		assertFalse(building.getFlats().contains(flatToDelete));
	}

	@Test(expected = FlatExistenceException.class)
	public void shouldThrowFlatExistenceExceptionWhileDeletingFlat() {

		// given
		FlatEntity flatToDelete = getExampleFlat();
		flatToDelete.setId(100L);

		// when
		serviceUnderTest.delete(flatToDelete);

		// then
		fail("FlatExistenceException should have thrown");
	}

	@Test
	public void shouldUpdateFlat() {

		// given
		FlatEntity flatToUpdate = serviceUnderTest.findOne(1L);
		flatToUpdate.setNumberOfRooms(10);

		// when
		serviceUnderTest.update(flatToUpdate);

		// then
		assertEquals(serviceUnderTest.findOne(1L).getNumberOfRooms(), flatToUpdate.getNumberOfRooms());
	}

	@Test(expected = ObjectOptimisticLockingFailureException.class)
	public void testShouldThrowOptimisticLockingExceptionOnCarUpdate() {

		// given
		FlatEntity firstFlat = serviceUnderTest.findOne(1L);
		FlatEntity secondFlat = serviceUnderTest.findOne(1L);

		// when
		firstFlat.setFloorNumber(5);
		entityManager.detach(firstFlat);

		secondFlat.setFloorNumber(6);
		entityManager.detach(secondFlat);

		serviceUnderTest.update(secondFlat);
		entityManager.flush();
		serviceUnderTest.update(firstFlat);

		// then
		fail("OptimisticLockException should have thrown!");
	}

	public Address getExampleAddress() {
		Address address = new Address();
		address.setBuildingNumber(1);
		address.setFlatNumber(1);
		address.setCity("Chicago");
		address.setStreet("Backer Street");
		address.setPostCode("10-100");
		return address;
	}

	public FlatEntity getExampleFlat() {
		FlatEntity flat = new FlatEntity();
		flat.setAddress(getExampleAddress());
		flat.setBuilding(buildingDao.findOne(1L));
		flat.setFloorNumber(1);
		flat.setNumberOfBalconies(0);
		flat.setNumberOfRooms(3);
		flat.setPrice(150000);
		flat.setStatus(Status.FREE);
		flat.setSizeInSquareMeters(100.0);
		return flat;
	}

	public Contact getExampleContact() {
		Contact contact = new Contact();
		contact.setMail("example@gmail.com");
		contact.setTelephone("111-111-111");
		return contact;
	}

	public ClientEntity getExampleClient() {
		ClientEntity client = new ClientEntity();
		client.setAddress(getExampleAddress());
		client.setContact(getExampleContact());
		client.setCreditCard("1111-1111-1111-1111");
		return client;
	}

}

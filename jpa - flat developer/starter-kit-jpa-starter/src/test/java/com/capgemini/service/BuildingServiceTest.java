package com.capgemini.service;

import java.util.List;

import static org.junit.Assert.*;
import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.domain.BuildingEntity;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class BuildingServiceTest {

	@Autowired
	private BuildingService serviceUnderTest;

	@Test
	public void shouldGetBuildingsWithBiggestNumberOfFreeFlats() {

		// given when
		List<BuildingEntity> buildings = serviceUnderTest.getBuildingWithTheBiggestNumberOfFreeFlats();
		BuildingEntity buildingWithIdOne = serviceUnderTest.findOne(1L);
		BuildingEntity buildingWithIdTwo = serviceUnderTest.findOne(2L);
		BuildingEntity buildingWithIdThree = serviceUnderTest.findOne(3L);
		BuildingEntity buildingWithIdFive = serviceUnderTest.findOne(5L);

		// then
		assertEquals(3, buildings.size());
		assertTrue(buildings.contains(buildingWithIdOne));
		assertTrue(buildings.contains(buildingWithIdThree));
		assertTrue(buildings.contains(buildingWithIdFive));
		assertFalse(buildings.contains(buildingWithIdTwo));
	}
}
package com.capgemini.datatype;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QLocalization is a Querydsl query type for Localization
 */
@Generated("com.querydsl.codegen.EmbeddableSerializer")
public class QLocalization extends BeanPath<Localization> {

    private static final long serialVersionUID = -857449489L;

    public static final QLocalization localization = new QLocalization("localization");

    public final StringPath city = createString("city");

    public final StringPath street = createString("street");

    public QLocalization(String variable) {
        super(Localization.class, forVariable(variable));
    }

    public QLocalization(Path<? extends Localization> path) {
        super(path.getType(), path.getMetadata());
    }

    public QLocalization(PathMetadata metadata) {
        super(Localization.class, metadata);
    }

}


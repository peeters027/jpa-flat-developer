package com.capgemini.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QClientEntity is a Querydsl query type for ClientEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QClientEntity extends EntityPathBase<ClientEntity> {

    private static final long serialVersionUID = 71561476L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QClientEntity clientEntity = new QClientEntity("clientEntity");

    public final QAbstractEntity _super = new QAbstractEntity(this);

    public final com.capgemini.datatype.QAddress address;

    public final com.capgemini.datatype.QContact contact;

    public final StringPath creditCard = createString("creditCard");

    public final ListPath<FlatEntity, QFlatEntity> flats = this.<FlatEntity, QFlatEntity>createList("flats", FlatEntity.class, QFlatEntity.class, PathInits.DIRECT2);

    //inherited
    public final NumberPath<Long> id = _super.id;

    //inherited
    public final NumberPath<Integer> version = _super.version;

    public QClientEntity(String variable) {
        this(ClientEntity.class, forVariable(variable), INITS);
    }

    public QClientEntity(Path<? extends ClientEntity> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QClientEntity(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QClientEntity(PathMetadata metadata, PathInits inits) {
        this(ClientEntity.class, metadata, inits);
    }

    public QClientEntity(Class<? extends ClientEntity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.address = inits.isInitialized("address") ? new com.capgemini.datatype.QAddress(forProperty("address")) : null;
        this.contact = inits.isInitialized("contact") ? new com.capgemini.datatype.QContact(forProperty("contact")) : null;
    }

}


package com.capgemini.domain;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QFlatEntity is a Querydsl query type for FlatEntity
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QFlatEntity extends EntityPathBase<FlatEntity> {

    private static final long serialVersionUID = 1973610418L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QFlatEntity flatEntity = new QFlatEntity("flatEntity");

    public final QAbstractEntity _super = new QAbstractEntity(this);

    public final com.capgemini.datatype.QAddress address;

    public final QBuildingEntity building;

    public final ListPath<ClientEntity, QClientEntity> clients = this.<ClientEntity, QClientEntity>createList("clients", ClientEntity.class, QClientEntity.class, PathInits.DIRECT2);

    public final NumberPath<Integer> floorNumber = createNumber("floorNumber", Integer.class);

    //inherited
    public final NumberPath<Long> id = _super.id;

    public final NumberPath<Integer> numberOfBalconies = createNumber("numberOfBalconies", Integer.class);

    public final NumberPath<Integer> numberOfRooms = createNumber("numberOfRooms", Integer.class);

    public final NumberPath<Double> price = createNumber("price", Double.class);

    public final NumberPath<Double> sizeInSquareMeters = createNumber("sizeInSquareMeters", Double.class);

    public final EnumPath<com.capgemini.datatype.Status> status = createEnum("status", com.capgemini.datatype.Status.class);

    //inherited
    public final NumberPath<Integer> version = _super.version;

    public QFlatEntity(String variable) {
        this(FlatEntity.class, forVariable(variable), INITS);
    }

    public QFlatEntity(Path<? extends FlatEntity> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QFlatEntity(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QFlatEntity(PathMetadata metadata, PathInits inits) {
        this(FlatEntity.class, metadata, inits);
    }

    public QFlatEntity(Class<? extends FlatEntity> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.address = inits.isInitialized("address") ? new com.capgemini.datatype.QAddress(forProperty("address")) : null;
        this.building = inits.isInitialized("building") ? new QBuildingEntity(forProperty("building"), inits.get("building")) : null;
    }

}


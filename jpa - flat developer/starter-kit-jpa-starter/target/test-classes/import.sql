INSERT INTO building(description,is_lift_equipped,city,street,number_of_flats,number_of_floors) VALUES (null,true,'Wrocław','Zachodnia',100,20);
INSERT INTO building(description,is_lift_equipped,city,street,number_of_flats,number_of_floors) VALUES (null,false,'Wrocław','Wielka',50,13);
INSERT INTO building(description,is_lift_equipped,city,street,number_of_flats,number_of_floors) VALUES (null,false,'Krotoszyn','Kobierska',40,10);
INSERT INTO building(description,is_lift_equipped,city,street,number_of_flats,number_of_floors) VALUES (null,true,'Poznań','Szeroka',66,15);
INSERT INTO building(description,is_lift_equipped,city,street,number_of_flats,number_of_floors) VALUES (null,false,'Gdańsk','Parkowa',49,6);
INSERT INTO building(description,is_lift_equipped,city,street,number_of_flats,number_of_floors) VALUES (null,false,'Warszawa','Morska',90,4);

INSERT INTO clients(building_number,city,flat_number,post_code,street,mail,telephone,credit_card) VALUES (10,'Konin',3,'63-700','Kaszarska','karol@gmail.com','123-123-123','1111-1111-1111-1111');
INSERT INTO clients(building_number,city,flat_number,post_code,street,mail,telephone,credit_card) VALUES (7,'Opole',8,'61-856','Wschodnia','per.inceptos.hymenaeos@incursus.net','776-707-577','5212-3608-9269');
INSERT INTO clients(building_number,city,flat_number,post_code,street,mail,telephone,credit_card) VALUES (7,'Olsztyn',14,'36-162','Południowa','nec.cursus.a@pharetrautpharetra.edu','600-026-388','4420-9596-4614');
INSERT INTO clients(building_number,city,flat_number,post_code,street,mail,telephone,credit_card) VALUES (17,'Kraków',19,'67-835','Północna','rhoncus.Donec@felispurusac.com','663-896-828','7467-8935-4464');
INSERT INTO clients(building_number,city,flat_number,post_code,street,mail,telephone,credit_card) VALUES (10,'Koło',16,'30-561','Mała','semper.et@vulputate.com','553-735-064','2573-5727-1192');
INSERT INTO clients(building_number,city,flat_number,post_code,street,mail,telephone,credit_card) VALUES (16,'Wałków',15,'80-534','Trawiasta','ut.mi@velit.net','667-916-692','3088-2575-1840');
INSERT INTO clients(building_number,city,flat_number,post_code,street,mail,telephone,credit_card) VALUES (18,'Lutogniew',19,'00-387','Średnia','Praesent@pharetraQuisque.net','644-125-334','6116-6532-2784');
INSERT INTO clients(building_number,city,flat_number,post_code,street,mail,telephone,credit_card) VALUES (4,'Łódź',15,'30-575','Ogromna','erat.neque@Etiamvestibulummassa.net','099-389-957','1884-6281-7964');

INSERT INTO flats(building_number,city,flat_number,post_code,street,floor_number,number_of_balconies,number_of_rooms,price,size,status,building_id) VALUES (10,'Krotoszyn',3,'63-700','Kobierska',3,3,3,100000,52.3,'FREE',3);
INSERT INTO flats(building_number,city,flat_number,post_code,street,floor_number,number_of_balconies,number_of_rooms,price,size,status,building_id) VALUES (6,'Wrocław',2,'50-100','Zachodnia',8,1,3,256662.10,82.00,'SOLD',1);
INSERT INTO flats(building_number,city,flat_number,post_code,street,floor_number,number_of_balconies,number_of_rooms,price,size,status,building_id) VALUES (18,'Wrocław',11,'50-200','Wielka',0,2,4,299137.00,57.00,'SOLD',2);
INSERT INTO flats(building_number,city,flat_number,post_code,street,floor_number,number_of_balconies,number_of_rooms,price,size,status,building_id) VALUES (17,'Krotoszyn',9,'63-700','Kobierska',2,0,3,202492.00,54.00,'RESERVED',3);
INSERT INTO flats(building_number,city,flat_number,post_code,street,floor_number,number_of_balconies,number_of_rooms,price,size,status,building_id) VALUES (6,'Poznań',11,'62-500','Szeroka',3,3,3,244978.00,61.00,'SOLD',4);
INSERT INTO flats(building_number,city,flat_number,post_code,street,floor_number,number_of_balconies,number_of_rooms,price,size,status,building_id) VALUES (11,'Gdańsk',6,'67-109','Parkowa',9,1,5,229521.00,45.00,'FREE',5);
INSERT INTO flats(building_number,city,flat_number,post_code,street,floor_number,number_of_balconies,number_of_rooms,price,size,status,building_id) VALUES (14,'Warszawa',20,'69-100','Morska',6,2,2,203290.00,40.00,'RESERVED',6);
INSERT INTO flats(building_number,city,flat_number,post_code,street,floor_number,number_of_balconies,number_of_rooms,price,size,status,building_id) VALUES (6,'Wrocław',9,'50-100','Zachodnia',4,1,2,256265.00,67.00,'RESERVED',1);
INSERT INTO flats(building_number,city,flat_number,post_code,street,floor_number,number_of_balconies,number_of_rooms,price,size,status,building_id) VALUES (16,'Wrocław',6,'50-100','Zachodnia',1,1,4,218945.00,98.00,'FREE',1);
INSERT INTO flats(building_number,city,flat_number,post_code,street,floor_number,number_of_balconies,number_of_rooms,price,size,status,building_id) VALUES (6,'Wrocław',5,'50-200','Wielka',4,2,5,292455.00,73.00,'RESERVED',2);

INSERT INTO building_flats(building_entity_id, flats_id) VALUES (1,2);
INSERT INTO building_flats(building_entity_id, flats_id) VALUES (1,8);
INSERT INTO building_flats(building_entity_id, flats_id) VALUES (1,9);
INSERT INTO building_flats(building_entity_id, flats_id) VALUES (2,3);
INSERT INTO building_flats(building_entity_id, flats_id) VALUES (2,10);
INSERT INTO building_flats(building_entity_id, flats_id) VALUES (3,1);
INSERT INTO building_flats(building_entity_id, flats_id) VALUES (3,4);
INSERT INTO building_flats(building_entity_id, flats_id) VALUES (4,5);
INSERT INTO building_flats(building_entity_id, flats_id) VALUES (5,6);
INSERT INTO building_flats(building_entity_id, flats_id) VALUES (6,7);

INSERT INTO clients_flats(clients_id, flats_id) VALUES (1,2);
INSERT INTO clients_flats(clients_id, flats_id) VALUES (1,3);
INSERT INTO clients_flats(clients_id, flats_id) VALUES (2,4);
INSERT INTO clients_flats(clients_id, flats_id) VALUES (3,5);
INSERT INTO clients_flats(clients_id, flats_id) VALUES (5,7);
INSERT INTO clients_flats(clients_id, flats_id) VALUES (6,8);
INSERT INTO clients_flats(clients_id, flats_id) VALUES (8,10);
